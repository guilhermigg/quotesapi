import express from 'express';

import * as fs from 'fs';
import path from 'path';

import dotenv from 'dotenv'
dotenv.config();

import databaseConnection from './database/connection';
databaseConnection();

const PORT = process.env.PORT || 8080;

const app = express();

app.disable('x-powered-by');
app.use(express.json());

// Use all files inside routes folder dinamically
var filesPath:string = path.join(__dirname, 'routes');

fs.readdir(filesPath, (err, files) => {
    if(err) return console.log(err);

    files.forEach(file => {
        let routerPath = path.join(filesPath, file);
        let router = require(routerPath);
        app.use(router);
    })
});

app.listen(PORT, () => {
    console.log(`The server is listening on http://localhost:${PORT}`);
});
