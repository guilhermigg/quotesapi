import { check, ValidationChain } from 'express-validator';

export const userValidator:Array<ValidationChain> = [
    check('email')
        .trim()
        .normalizeEmail()
        .isEmpty()
        .isEmail(),

    check('password')
        .trim()
        .isLength({min: 8})
]