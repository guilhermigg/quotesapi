import { Router } from 'express';
const router = Router();

import mainController from '@controllers/mainController';

router.get('/', mainController.main);

module.exports = router;