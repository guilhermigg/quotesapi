import { Router } from 'express';
const router = Router();

import userController from '@controllers/userController.js';
import { userValidator } from '../helpers/userValidator';

router.get('/auth', userController.userAuth);
router.post('/user', userValidator, userController.userRegister);

module.exports = router;