import * as mongoose from 'mongoose';

interface IUser {
    email: string;
    password: string;
    type: number;
  }

const userSchema = new mongoose.Schema<IUser>({
    email: {type: String, required: true},
    password: {type: String, required: true},
    type: {type: Number, required: true},
});

const userModel = mongoose.model<IUser>('User', userSchema);

export {userModel};