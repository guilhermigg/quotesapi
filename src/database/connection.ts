import { connect } from 'mongoose';
import 'dotenv';

interface iConfig {
    user: string;
    password: string;
    port: string;
    ip: string;
}

const config = <iConfig> {
    user: process.env.MONGO_INITDB_ROOT_USERNAME,
    password: process.env.MONGO_INITDB_ROOT_PASSWORD,
    port: process.env.MONGO_PORT || 27017,
    ip: process.env.MONGO_IP || "mongo"
};

export default function databaseConnection(){
    const url:string = `mongodb://${config.user}:${config.password}@${config.ip}:${config.port}/?authSource=admin`;

    return connect(url)
        .then(() => console.log("[Database] Successfully connected"))
        .catch((error:Error) => console.log("[Database] "+error));
}
