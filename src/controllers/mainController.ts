import { Request, Response } from 'express';

export default {
    async main (req: Request, res: Response) {
        return res.json({
            "msg": "Welcome to the QuotesAPI",
            "account": "Create an account making a POST request to /user"
        });
    }
};