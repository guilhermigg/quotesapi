import { Request, Response } from 'express';
import { userModel } from '@models/User';
import * as bcrypt from 'bcrypt';

const saltRounds:number = 10;

class userController {
    async userAuth (req: Request, res: Response) {
        const email = req.body["email"];

        const user = await userModel.findOne({email});
        if(!user) return res.sendStatus(404)
        // const unhashedPassword = await bcrypt.compareSync(password, userPassword)

        return res.sendStatus(200)
    }

    async userRegister (req: Request, res: Response) {
        const { email, password } = req.body;

        if(!email || !password) return res.json({
            error: true,
            msg: "Email ou senha não foram passados"
        });

        if(password.trim().length < 8) return res.json({
            error: true,
            msg: "Password too short! Password must be 8 or more characters long."
        })

        const user = await userModel.findOne({email});

        if(user) return res.status(200).json({
            msg: "Email já está registrado",
            error:true
        });

        const hashedPassword = await bcrypt.hashSync(password, saltRounds);

        await userModel.create({
            email,
            hashedPassword
        });

        return res.json({
            msg: "Account created with success",
            error: false
        });
    }
}


export default new userController();