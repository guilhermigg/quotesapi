FROM node:15
WORKDIR /app
COPY package*.json ./

ARG NODE_ENV
RUN if [ "$NODE_ENV" = "development" ]; \
        then npm ci; \
        else npm ci --only=production; \
        fi

RUN npm ci
COPY . .
ENV PORT=8080
EXPOSE $PORT
CMD ["node", "index.js"]
