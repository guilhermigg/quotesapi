# QuotesAPI

```js
const project = {
    "description": "This project was made to practice and learn new technologies.",
    "author": "Guilherme Evaristo",
    "techs": ["TypeScript", "Express", "MongoDB", "Docker"],
    "year": 2022,
    "complete": false
};
```

## Running the app...

**1. Clone this repository:**
`git clone git@gitlab.com:guilhermigg/quotesapi.git`

**2. Access the downloaded folder and install the necessary packages with npm**
`npm -ci`

**3. Rename example.env to .env and add your info**
`mv example.env .env`

**4. Run the application with the npm script:**
`npm run dev`

**5. Be happy :D**
